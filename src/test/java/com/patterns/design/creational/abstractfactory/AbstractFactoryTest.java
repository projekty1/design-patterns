package com.patterns.design.creational.abstractfactory;

import com.patterns.design.creational.abstractfactory.factories.GUIFactory;
import com.patterns.design.creational.abstractfactory.factories.MacOSFactory;
import com.patterns.design.creational.abstractfactory.factories.WindowsFactory;
import org.junit.jupiter.api.Test;


class AbstractFactoryTest {

    private Application configureApplication() {
        Application app;
        GUIFactory factory;
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("mac")) {
            factory = new MacOSFactory();
            app = new Application(factory);
        } else {
            factory = new WindowsFactory();
            app = new Application(factory);
        }
        return app;
    }

    @Test
    public void testAbstractFactory() {
        Application app = configureApplication();
        app.paint();
    }
}