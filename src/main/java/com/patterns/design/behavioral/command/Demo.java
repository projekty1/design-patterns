package com.patterns.design.behavioral.command;

import com.patterns.design.behavioral.command.editor.Editor;

public class Demo {
    public static void main(String[] args) {
        Editor editor = new Editor();
        editor.init();
    }
}