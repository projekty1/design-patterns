package com.patterns.design.behavioral.memento.commands;

public interface Command {
    String getName();
    void execute();
}