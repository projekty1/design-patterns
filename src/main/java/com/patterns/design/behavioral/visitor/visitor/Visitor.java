package com.patterns.design.behavioral.visitor.visitor;

import com.patterns.design.behavioral.visitor.shapes.Circle;
import com.patterns.design.behavioral.visitor.shapes.CompoundShape;
import com.patterns.design.behavioral.visitor.shapes.Dot;
import com.patterns.design.behavioral.visitor.shapes.Rectangle;

public interface Visitor {
    String visitDot(Dot dot);

    String visitCircle(Circle circle);

    String visitRectangle(Rectangle rectangle);

    String visitCompoundGraphic(CompoundShape cg);
}