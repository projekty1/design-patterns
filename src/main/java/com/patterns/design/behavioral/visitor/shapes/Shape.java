package com.patterns.design.behavioral.visitor.shapes;

import com.patterns.design.behavioral.visitor.visitor.Visitor;

public interface Shape {
    void move(int x, int y);
    void draw();
    String accept(Visitor visitor);
}