package com.patterns.design.creational.builder.cars;

public enum Type {
    CITY_CAR, SPORTS_CAR, SUV
}