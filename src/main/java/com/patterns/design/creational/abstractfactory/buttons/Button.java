package com.patterns.design.creational.abstractfactory.buttons;

public interface Button {
    void paint();
}
