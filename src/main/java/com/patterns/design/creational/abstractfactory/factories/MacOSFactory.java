package com.patterns.design.creational.abstractfactory.factories;

import com.patterns.design.creational.abstractfactory.buttons.Button;
import com.patterns.design.creational.abstractfactory.buttons.MacOSButton;
import com.patterns.design.creational.abstractfactory.checkboxes.Checkbox;
import com.patterns.design.creational.abstractfactory.checkboxes.MacOSCheckbox;

public class MacOSFactory implements GUIFactory {

    @Override
    public Button createButton() {
        return new MacOSButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new MacOSCheckbox();
    }
}
