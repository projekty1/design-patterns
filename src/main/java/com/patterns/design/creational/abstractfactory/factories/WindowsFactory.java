package com.patterns.design.creational.abstractfactory.factories;

import com.patterns.design.creational.abstractfactory.buttons.Button;
import com.patterns.design.creational.abstractfactory.buttons.WindowsButton;
import com.patterns.design.creational.abstractfactory.checkboxes.Checkbox;
import com.patterns.design.creational.abstractfactory.checkboxes.WindowsCheckbox;

public class WindowsFactory implements GUIFactory {

    @Override
    public Button createButton() {
        return new WindowsButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new WindowsCheckbox();
    }
}