package com.patterns.design.creational.abstractfactory;

import com.patterns.design.creational.abstractfactory.buttons.Button;
import com.patterns.design.creational.abstractfactory.checkboxes.Checkbox;
import com.patterns.design.creational.abstractfactory.factories.GUIFactory;

public class Application {
    private Button button;
    private Checkbox checkbox;

    public Application(GUIFactory factory) {
        button = factory.createButton();
        checkbox = factory.createCheckbox();
    }

    public void paint() {
        button.paint();
        checkbox.paint();
    }
}