package com.patterns.design.creational.abstractfactory.factories;

import com.patterns.design.creational.abstractfactory.buttons.Button;
import com.patterns.design.creational.abstractfactory.checkboxes.Checkbox;

public interface GUIFactory {
    Button createButton();
    Checkbox createCheckbox();
}