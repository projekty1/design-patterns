package com.patterns.design.creational.abstractfactory.checkboxes;

public interface Checkbox {
    void paint();
}