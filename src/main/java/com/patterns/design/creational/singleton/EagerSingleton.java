package com.patterns.design.creational.singleton;

public class EagerSingleton {
    //the variable will be created when the class is loaded
    private static final EagerSingleton instance = new EagerSingleton();
    private EagerSingleton(){}
    public static EagerSingleton getInstance(){
        return instance;
    }
}
