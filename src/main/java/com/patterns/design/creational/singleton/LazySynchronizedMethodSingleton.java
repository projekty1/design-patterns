package com.patterns.design.creational.singleton;

public class LazySynchronizedMethodSingleton {
    private static LazySynchronizedMethodSingleton INSTANCE = null;
    private LazySynchronizedMethodSingleton() {}
    public synchronized static LazySynchronizedMethodSingleton getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new LazySynchronizedMethodSingleton();
        }
        return INSTANCE;
    }
}
