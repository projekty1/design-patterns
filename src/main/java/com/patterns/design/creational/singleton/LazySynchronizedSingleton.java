package com.patterns.design.creational.singleton;

public class LazySynchronizedSingleton {
    private static LazySynchronizedSingleton INSTANCE = null;
    private LazySynchronizedSingleton() {}
    public static LazySynchronizedSingleton getInstance() {
        if (INSTANCE == null) {
            synchronized (LazySynchronizedSingleton.class) {
                if (INSTANCE == null) {
                    INSTANCE = new LazySynchronizedSingleton();
                }
            }
        }
        return INSTANCE;
    }
}