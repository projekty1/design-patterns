package com.patterns.design.creational.singleton;

class HelperClassSingleton {
    private static class SingletonHolder {
        public static HelperClassSingleton instance = new HelperClassSingleton();
    }

    public static HelperClassSingleton getInstance() {
        return SingletonHolder.instance;
    }
}