package com.patterns.design.creational.factorymethod.factory;

import com.patterns.design.creational.factorymethod.buttons.Button;
import com.patterns.design.creational.factorymethod.buttons.WindowsButton;

/**
 * Windows Dialog will produce Windows buttons.
 */
public class WindowsDialog extends Dialog {

    @Override
    public Button createButton() {
        return new WindowsButton();
    }
}