package com.patterns.design.creational.factorymethod.factory;

import com.patterns.design.creational.factorymethod.buttons.Button;
import com.patterns.design.creational.factorymethod.buttons.HtmlButton;

/**
 * HTML Dialog will produce HTML buttons.
 */
public class HtmlDialog extends Dialog {

    @Override
    public Button createButton() {
        return new HtmlButton();
    }
}