package com.patterns.design.structural.facade.some_complex_lib;

public class OggCompressionCodec implements Codec {
    public String type = "ogg";
}